~~~
T: Website meta.
D: A guide to the colours and images I use for this website, more as a remider to myself than anything else.
O: web_meta.html
#: meta
~~~

## Colours

These are the colours used on this website.

* Background #272727
* foreground #ededed
* highlight #f449d5

## Images for web

Favicon.
=> ./media/icons/favicon.png A small pink square.
Logo big.
=> ./media/images/branding/500.png A big pink square.
Logo text.
=> ./media/images/branding/500-text.png A big pink square with james chip dot io written inside it.
Logo under text light.
=> ./media/images/branding/500-light-text.png A big pink square with james chip dot io under it.
Logo under text dark.
=> ./media/images/branding/500-dark-text.png A big pink square with james chip dot io under it.

## Images for print

This is the closes CMYK version of my logs, because when I first did my branding I did not take printing into consideration while choosing the colour. Opps.

Logo cmyk.
=> ./media/images/branding/500-cmyk.png A big pink square.

