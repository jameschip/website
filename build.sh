#! /bin/bash

SITE_BUILD=_site
SITE_TMP=_tmp

function clean {
	printf "Cleaning output.\n"
	if [ -d "$SITE_BUILD" ]; then
		rm -f "$SITE_BUILD/"*.html
	fi

	if [ -d "$SITE_TMP" ]; then
		rm -rf "$SITE_TEMP"
	fi

	
}

function build {
	printf "Building website.\n"
	mardle
	cp -r css "$SITE_BUILD"
	cp -r media "$SITE_BUILD"
	cp index.html "$SITE_BUILD"
	cp -r adventurer_online	"$SITE_BUILD"
	cp index.xml "$SITE_BUILD"
}

function debug {
	printf "Running busybox httpd server.\n"
	printf "Visit localhost:8080 with your browser to test.\n"
	busybox httpd -f -p 8080 -v -h ./_site
}

clean
build


while getopts "d" opt; do
	case "$opt" in
		d)
			debug
			;;
		?)
			echo "Usage: $(basename $0) [-d]"
			exit 1
			;;
	esac
done



