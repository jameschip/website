~~~
T: Growing potatoes
D: Notes on growing potatoes
O: growing_potaoes.html
#: gardening
~~~
=> ./media/images/gardening/potato.jpg A photo of my hand holding a single potato covered in soil. Potato plants in sacks can be seen in the background.

While there are countless varieties of potato to grow they all fall into one of the following groups:

* First earlies
* Second earlies
* Maincrop

First and second earlies are your smaller new and salad potato types, while maincrop are your larger potatoes that you keep for the winter months.

First and second earlies are ready long before main crops will be. The benefit of this is that sometimes, if you get an early crop, you can plant something else out, like leeks, where they came out.

It is not recommended to grow from potatoes that you get from the supermarket. These potatoes have likely been sprayed to prevent, or slow down, their ability to sprout; leading to a very poor yield. They are also not guaranteed to be blight free, meaning you may introduce blight to your garden. It is best to grow from good seed potatoes from a reputable source. You can, if you wish, then make seed potatoes of your own for the next year from your first crop. 

## chitting

Chitting is putting the spuds out to form shoots before you plant them. In February get your seed potatoes and place them in a cool spot out of direct sunlight where they wont be disturbed. Shoots will form over a few weeks. Once the shoots are about an inch long the spuds can be planted, if conditions are good.

While chitting isn't mandatory, it is a good idea with first and second earlies as it gives them a head start, but with main crops it is fairly pointless.

Before planting first earlies you should rub off all but the 4 strongest shoots, which causes the growth to be faster. With second earlies and maincrop, it isn't necessary to do this, but it wont hurt.

## planting out

First earlies should be planted around the end of March, and will be ready to crop in June or July. Second earlies should be planted around mid April, and will be ready to crop in July or August. Main crop should go in at the very end of April to the start of May, and will be ready to crop from late August to October.

When you plant your spuds out is highly dictated by the temperature of the soil. The rule of thumb I use is that if you can bury your hand down in the soil, keep it there for about 20 seconds, and it doesn't feel uncomfortably cold then you are good to go.

Plant in a good sunny spot to avoid late frosts damaging young shoots.

### planting in the ground

Dig a trench about the depth of a trowel and place the seed potatoes in about 30cm apart, then cover them completely in soil. Job done.

Make sure to change the location you plant potatoes in the ground each year to avoid pest build up. Crop rotation, it is important.

### planting in tubs

Get a tub that is 30x30 or bigger and fill it half up with soil and set potatoes just below the surface. You don,t want to crowd them in too much so in a 30x30 pot you want to plant two, well spaced, at a push.

First and second earlies grow well in tubs because the plants are smaller. Main crops can be grown in tubs but it isn't really the best idea, you wont get a big yield.

## earthing up

Once plants have reached about 15cm high it is time to earth up. Get extra soil and heap it up around your plants to bury all the the top most leaves.

This will nit only protect tubers already grown from being exposed from the sun, turning them green and ineddible, but it also encorages the plant to produce more tubers from the shoots you have burried.

You will need to earth up once more when the plant grows to a similar height. After that just let the plant grow.

## harvesting

First earlies are usually ready when the flowers appear. Second earlies usually when the flowers have gone over.

For maincrop wait until the plant yellows and cut the plant off leaving the potatoes in the ground. A week later dig up the potatoes and leave them on the soil to dry out for a few hours, then store.

Before digging up plants you can remove some soil by hand and carefully check some tubers to see if they are ready. Cover back over if they are not ready, or go on a treasure hunt if they are!

## problems

These are some of the common problems faced when growing spuds.

### blight

Blight is a fungal infection that will kill your plants and cause tubers to rot. It lives in the soil so uf you get it chances are you will have it next year too. Blight is much more common at the end of summer when it us warm and wet, so earlies aren't so often affected as main crops.

It looks like brown blotchy stems that grow and eventually kill the plant.

Dispose of blight affected plants in the bin or through fire to avoid spreading it. Do not compost blighted plants.

There isn't much you can do about blight other than try growing early and blight resistant varieties.

### scab

Scab forms brown, patchy scabs on the tubers. The tubers are fine to eat after peeling. Not much can be done about scab, if it comes back every year then just move where you plant your spuds.

### slugs

Slugs will chomp on your spuds, especially maincrops left in the ground. Slugs are just a thing that exist and you have to come to terms with that. I do not condone the use of pesticides to control slugs.

### hollow potatoes

You wont know about this until it is too late. The problem here is that your potatoes grew too fast at some point. This is usually caused by under watering and feeding for a period of time and then suddenly over watering to compensate.

In the period of time where you have not been watering potato growth will slow to a crawl. When you start watering vigorously again the tubers start to swell fast and basically rip themselves apart in the centre as they try to keep up.

The only thing that can be done about this is to maintain a good watering practice to keep your soil at a roughly even moisture. If it rains a lot then don't water for a couple of days. If it is hot and dry, water more frequently.

Just dont let them dry out, then water them lots to compensate.
