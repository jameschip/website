#! /bin/bash

function ext_exists () ( 
    ext="$1"
    any=false
    shopt -s nullglob
    for f in *."$ext"; do
        any=true
        break
    done
    echo $any 
)

function optimise {
	if $(ext_exists jpg) ; then 
		for X in *.jpg; do magick convert "$X" -resize 700 -strip -quality 86 "$X"; done
	fi

	if $(ext_exists jpeg) ; then
		for X in *.jpeg; do magick convert "$X" -resize 700 -strip -quality 86 "$X"; done
	fi

	if $(ext_exists png) ; then
		for X in *.png; do magick convert "$X" -resize 700 -strip -quality 86 "$X"; done
	fi
}

if [ -d media ]; then
	rm -rf media
fi

cp -r _media media

cd media/images/drawing
optimise

cd ../games
optimise

cd ../software
optimise
