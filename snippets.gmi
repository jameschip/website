~~~
T: Snipptes
D: Little snippets of stuff I often need, but dont commit to memory.
O: snippets.html
#: software notes
#: snippets
~~~

Here are a bunch of things that I sometimes need, but don't ever quite commit to memory and I keep having to look up.

## Rsync upload with permissions

Upload files and directories to a remote with nice permissions.

```
rsync -azP --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r . <user>@<ip>:</var/www/html/>
```

