~~~
T: Theoretically Infinite Naughts and Crosses
D: Theoretically Infinite Naughts and Crosses
O: tinc.html
#: games
#: projects
~~~

In the standard version on Noughts and Crosses if each player makes optimal moves every game will end in a draw. TINC differs from this in the way that if both players make optimal moves, theoretically, the game could carry on to be infinitely big and continue being played until the end of time.

## How?

A turn in a game of TINC has three phases.

* Place up to two symbols on the grid.
* Check for a win.
* Expand the grid if needed. 

To win a player must create an unbroken line, orthogonal or diagonal, of more than one of their symbol from one side of the grid to the other. Players go back and forth taking turns trying to win.

## Setup

* Choose who will be noughts and who will be crosses.
* Draw a single square cell in the centre of the paper, this is the grid. 

## Place up to two symbols

* During their turn a player places up to two of their symbol in an empty cell on the grid.
* A symbol may only be placed in an empty cell.
* The player must place at least one symbol on their turn.
* If there is only one cell available then the player must place their symbol in that cell and may not place any more symbols this turn.
* You may not place a symbol of the other players type. 

## Check for win

After you have placed your symbols on the grid you then check to see if either player has won. If a player has won then the game ends here, do not proceed to the next phase.

## Expand grid

If at the end of a turn no player has won and there is a symbol of either type touching the edge of the grid then the grid expands. Grow the grid by one cell in each direction.

## Play Example 

In this example an X is a cross that has been placed in a cell a O is a nought that has been places in a cell and a ~ is an empty cell.

### Turn one

On this turn there is only one space available, the player must place their symbol here. 
```

	~	->		X

```

We can see at this point the player has not won so we check to grow the grid. As there is a symbol against the edge of the grid then we expand it one cell in each direction.
```
				~~~
	X	->		~X~
				~~~
```

### Turn two

On their turn the next player may place up to two of their symbols on the grid.
```
	~~~				~~~
	~X~		->		~XO
	~~~				~~O
```

Again we check to see if either player has won. As they have not we grow the grid again!
``` 
					~~~~~
	~~~				~~~~~
	~XO		->		~~XO~		
	~~O		 		~~~O~
					~~~~~
```

An the game continues as such.
